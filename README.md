Objects To New Layer - An Inkscape Extension

Inkscape 1.1 +

▶ Creates a new layer and copies or
  transfers the selected objects to
  that layer

▶ Appears in 'Extensions>Arrange'

▶ A shortcut can be used to
  org.inkscape.inklinea.objects_to_new_layer.noprefs
